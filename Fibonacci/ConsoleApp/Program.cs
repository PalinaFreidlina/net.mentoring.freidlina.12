﻿using System;
using System.Linq;
using FibonacciNumbers;
using FibonacciNumbers.Cache;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("1 - In-process cache; 2 - Out-of-process cache; other - exit");
                Console.Write("Enter a key: ");
                var key = Console.ReadLine();
                Console.WriteLine("Input count of numbers: ");

                if (!Int32.TryParse(Console.ReadLine(), out int count))
                {
                    Console.WriteLine("Invalid input");
                    Console.ReadKey();
                    continue;
                }
                
                switch (key)
                {
                    case "1":
                        MemoryCache(count);
                        break;
                    case "2":
                        RedisCache(count);
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        Console.ReadKey();
                        continue;
                }

                Console.WriteLine();
            }
        }

        private static void MemoryCache(int count)
        {
            var generator = new CachedFibonacciGenerator(new FibonacciGenerator(), new FibonacciMemoryCache());
            generator.Get(count).ToList().ForEach(Console.WriteLine);
        }

        private static void RedisCache(int count)
        {
            var generator = new CachedFibonacciGenerator(new FibonacciGenerator(), new FibonacciRedisCache("localhost"));
            generator.Get(count).ToList().ForEach(Console.WriteLine);
        }
    }
}
