﻿using System.Collections.Generic;

namespace FibonacciNumbers.Cache
{
    public interface ICache
    {
        IEnumerable<int> Get(int forCount);
        IEnumerable<int> Get(string forUser);

        void Set(string forUser, IEnumerable<int> numbers);
        void Set(int forCount, IEnumerable<int> numbers);
    }
}
